const list = document.querySelector('ul');
const input = document.querySelector('input');
const add_item_btn = document.querySelector('#add_item_btn');

function add_item_to_list(){
    const item = input.value;
    input.value = '';

    const li = document.createElement('li');
    const span = document.createElement('span');
    const button = document.createElement('button');
    li.appendChild(span);
    li.appendChild(button);
    span.textContent = item;
    button.textContent = 'Delete';

    list.appendChild(li);

    button.addEventListener(
        'click',
        function (){
            list.removeChild(li);
        }
    );

    input.focus();
}

add_item_btn.addEventListener(
    'click',
    add_item_to_list
);
