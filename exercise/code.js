const container = document.querySelector('#container');

const content = document.createElement('div');
content.classList.add('content');
content.textContent = 'This is the glorious text-content!';
container.appendChild(content);

// a <p> with red text saying 'Hey I'm red!'
const p = document.createElement('p')
p.style.color = 'red';
p.textContent = 'Hey I\'m red!';
container.appendChild(p);

// an <h3> with blue text that says "I'm a blue h3!"
const h3 = document.createElement('h3')
h3.style.color = 'blue';
h3.textContent = 'I\'m a blue h3!';
container.appendChild(h3);

// a <div> with a black border and pink background color
const div = document.createElement('div')
div.style.border = '2px solid black';
div.style.backgroundColor = 'pink';
container.appendChild(div);

const div_h1 = document.createElement('h1');
div_h1.textContent = 'I\'m in a div';
div.appendChild(div_h1);

const div_p = document.createElement('p');
div_p.textContent = 'ME TOO!';
div.appendChild(div_p);


const js_onclick_property_button = document.querySelector(
    '#js_onclick_property'
);
js_onclick_property_button.onclick = () => alert('Hello World from JS!');

const js_event_listener_button = document.querySelector(
    '#js_event_listener'
);
js_event_listener_button.addEventListener(
    'click',
    () => alert('Hello World from JS using event listener!')
);

const print_event_button = document.querySelector(
    '#print_event'
);
print_event_button.addEventListener(
    'click',
    function (e) {
        console.log(e.target);
        e.target.style.backgroundColor = 'blue';
    }
)
